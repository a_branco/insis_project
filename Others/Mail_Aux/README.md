# README
Place activiti.xml file in the <EI_HOME>/wso2/business-process/conf/ directory.
Place the three .jar files in <EI_HOME>/lib directory.

# NOTE
Sending emails will not be possible unless the JDK in use by the Business Process is the JDK8.