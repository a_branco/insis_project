package org.wso2.bpmn.populainformacaopolosservicetask.v1;

public class Obra {
	
	private long id;
	private String estadoRequisicao;
	private String estadoConservacao;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEstadoRequisicao() {
		return estadoRequisicao;
	}
	public void setEstadoRequisicao(String estadoRequisicao) {
		this.estadoRequisicao = estadoRequisicao;
	}
	public String getEstadoConservacao() {
		return estadoConservacao;
	}
	public void setEstadoConservacao(String estadoConservacao) {
		this.estadoConservacao = estadoConservacao;
	}
	
	public Obra(long id, String estadoRequisicao, String estadoConservacao) {
		super();
		this.id = id;
		this.estadoRequisicao = estadoRequisicao;
		this.estadoConservacao = estadoConservacao;
	}
	
	public Obra() {
	}

}
