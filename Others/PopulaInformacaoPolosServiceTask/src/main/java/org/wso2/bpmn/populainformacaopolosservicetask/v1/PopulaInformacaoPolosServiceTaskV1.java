package org.wso2.bpmn.populainformacaopolosservicetask.v1;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class PopulaInformacaoPolosServiceTaskV1 implements JavaDelegate {

	private final String INFOPOLOS = "infoPolos";
	
	@SuppressWarnings("unchecked")
	public void execute(DelegateExecution execution) throws Exception {
		
		if (execution.hasVariable(INFOPOLOS));
		List<InfoPolos> infoPolosList = execution.getVariable(INFOPOLOS, List.class);
		for (InfoPolos infoPolos : infoPolosList) {
			infoPolos.setnObras(infoPolos.getObras().size());
		}
		
		execution.setVariable(INFOPOLOS, infoPolosList);
		
	}

}
