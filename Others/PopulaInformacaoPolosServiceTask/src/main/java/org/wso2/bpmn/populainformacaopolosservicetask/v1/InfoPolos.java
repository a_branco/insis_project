package org.wso2.bpmn.populainformacaopolosservicetask.v1;

import java.util.ArrayList;

public class InfoPolos {
	
	private long id;
	private String nome;
	private String localizacao;
	private ArrayList<Obra> obras;
	private int nObras;
	
	public static final String TYPE_NAME = "simpleSelect";

	public String getName() {
		return TYPE_NAME;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public ArrayList<Obra> getObras() {
		return obras;
	}

	public void setObras(ArrayList<Obra> obras) {
		this.obras = obras;
	}

	public int getnObras() {
		return nObras;
	}

	public void setnObras(int nObras) {
		this.nObras = nObras;
	}

	public InfoPolos() {
	}

	public InfoPolos(long id, String nome, String localizacao, ArrayList<Obra> obras) {
		super();
		this.id = id;
		this.nome = nome;
		this.localizacao = localizacao;
		this.obras = obras;
	}

}
