package org.wso2.bpmn.listaperitos.v1.Configurations;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GetInfos implements JavaDelegate {

	private static Log log = LogFactory.getLog(GetInfos.class);

	public Expression variableNameExpression;
	
	private static final String POLOS = "polos";
	private static final String AREAS_CONHECIMENTO = "areasConhecimento";
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		// TODO Auto-generated method stub
		
		log.warn("Ocorreu um erro ao tentar ler a informação sobre os pólos. Serão usados os dados armazenados da última execução.");
		
		Path currentRelativePath = Paths.get("");
		String absolutePath = currentRelativePath.toAbsolutePath().toString();
		
		if(variableNameExpression.toString().equals("polos")) { // Ocorreu erro a executar tarefa rest que ia buscar a informação dos pólos utiliza os dados da ultima execução que foram armazenadas num ficheiro
		
			// Open do ficheiro e set da variável						        
	        String filenamePolos = absolutePath + "/conf/backup/list-polos.txt";
	        
		    RandomAccessFile reader = new RandomAccessFile(filenamePolos, "r");
		    FileChannel channel = reader.getChannel();
		 
		    int bufferSize = 1024;
		    if (bufferSize > channel.size()) {
		        bufferSize = (int) channel.size();
		    }
		    
		    ByteBuffer buff = ByteBuffer.allocate(bufferSize);
		    channel.read(buff);
		    buff.flip(); 
		    
		    String polos = new String(buff.array());
		    
		    log.info("Foram lidas as seguintes informações do ficheiro /config/backup/list-polos.txt");
		    log.info(polos);
		    channel.close();
		    reader.close();
		    
		    // É necessário fazer o set da informação liga numa variável polos
		    execution.setVariable(POLOS, polos); //
		}
		if(variableNameExpression.toString().equals("areasConhecimento")) {
			// Open do ficheiro e set da variável						        
	        String filenameAreasConhecimento = absolutePath + "/conf/backup/list-areasConhecimento.txt";
	        
		    RandomAccessFile reader = new RandomAccessFile(filenameAreasConhecimento, "r");
		    FileChannel channel = reader.getChannel();
		 
		    int bufferSize = 1024;
		    if (bufferSize > channel.size()) {
		        bufferSize = (int) channel.size();
		    }
		    
		    ByteBuffer buff = ByteBuffer.allocate(bufferSize);
		    channel.read(buff);
		    buff.flip(); 
		    
		    String areasConhecimento = new String(buff.array());
		    
		    log.info("Foram lidas as seguintes informações do ficheiro /config/backup/list-areasConhecimento.txt");
		    log.info(areasConhecimento);
		    channel.close();
		    reader.close();
		    
		    // É necessário fazer o set da informação liga numa variável polos
		    execution.setVariable(AREAS_CONHECIMENTO, areasConhecimento); //
		}
		
	}

}
