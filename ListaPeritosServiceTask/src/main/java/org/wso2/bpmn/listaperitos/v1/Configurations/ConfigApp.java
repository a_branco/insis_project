package org.wso2.bpmn.listaperitos.v1.Configurations;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger; 

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class ConfigApp implements JavaDelegate {

	private static final Logger LOGGER = Logger.getLogger(ConfigApp.class.getName());
	HashMap <String, String> properties = new HashMap<String, String>();
	
	@SuppressWarnings("rawtypes")
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		
		LOGGER.info("Config File Service Task");
		ConfigFile file = new ConfigFile();
		
		this.properties = file.getProperties();
		
		Iterator entries = this.properties.entrySet().iterator();
		while (entries.hasNext()) {
			HashMap.Entry entry = (HashMap.Entry) entries.next();
			String key = entry.getKey().toString();
			String value = entry.getValue().toString();
			
			execution.setVariable(key, value);
		}	
	}
}
