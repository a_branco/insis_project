package org.wso2.bpmn.listaperitos.v1;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.activiti.engine.delegate.Expression;

public class AgregaPareceres implements TaskListener {

	private static final long serialVersionUID = 1L;
	private static final String ASSIGNEE = "assignee";
	private static final String RESPOSTA_PERITO = "aprovaAquisicao";
	private static final String NUMBER_PERITOS = "numberPeritos";

	private static Log log = LogFactory.getLog(AgregaPareceres.class);

	public int nPareceresPositivos = 0;
	
	public Expression variableNameExpression;
	
	@Override
	public void notify(DelegateTask delegateTask) {
		log.info("gathering opinions...");

		List<String> respostasPeritos = new ArrayList<>();

		String resposta = delegateTask.getVariable(RESPOSTA_PERITO, String.class);
		String assignee = delegateTask.getVariable(ASSIGNEE, String.class);

		log.info(assignee + ": " + resposta);

		respostasPeritos.add(assignee + ": " + resposta);

		int numberPeritos = delegateTask.getVariable(NUMBER_PERITOS, Integer.class);
		log.info("Nº de peritos: " + numberPeritos);
		
		String peritosAprovam = "false";
		
		if(resposta.equals("true")){
			nPareceresPositivos++;
		}
		
		if(nPareceresPositivos > (numberPeritos/2)) {
			peritosAprovam = "true";
			log.info("Mais de metade dos peritos aprovaram a aquisição");
		}
		
		delegateTask.setVariable("peritosAprovam", peritosAprovam);
		delegateTask.setVariable("numberPareceresPositivos", nPareceresPositivos);

	}

}
