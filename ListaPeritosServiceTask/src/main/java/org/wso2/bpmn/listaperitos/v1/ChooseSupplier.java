package org.wso2.bpmn.listaperitos.v1;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Map;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.bpmn.listaperitos.v1.Configurations.GetInfos;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.*;

@SuppressWarnings("rawtypes")
public class ChooseSupplier implements JavaDelegate {
	
	public Expression variableNameExpression;
	private static Log log = LogFactory.getLog(ChooseSupplier.class);
	  HashMap<String, String> precos = new HashMap<String, String>();
	
	private static final String PRECOS_AQUISICAO = "precoAquisicao";
	private static final String PRECO_FINAL = "precoFinal";
	
	@Override
	public void execute(DelegateExecution delegateTask) throws Exception {
		// 
		String precosAquisicao = delegateTask.getVariable(PRECOS_AQUISICAO).toString();
		
		log.info(precosAquisicao);
		
		// Formatar a string que vem em JSON e criar um HashMap para fácil leitura
		String withoutFirstCharacter = precosAquisicao.substring(11); // index starts at zero
		String withoutLastCharacter = withoutFirstCharacter.substring(0, withoutFirstCharacter.length() - 2);
		
	
		String[] newPrecosAquisicao = withoutLastCharacter.split("\\},\\{");
		List<String> arrayFornecedores = Arrays.asList(newPrecosAquisicao); // Separa a string e criar um array com cada fornecedor e respetivo preço
		
		HashMap<String, Integer> newStructure = new HashMap<String, Integer>(); // HashMap que será a nova estrutura dos preços
		
		int count = 0;
		
		for (String fornecedor : arrayFornecedores) { // Percorre cada fornecedor
			
			String auxString = "";
			count++;
			if(count == 1) {
				auxString = fornecedor.substring(28, fornecedor.length());
			} else {
				auxString = fornecedor.substring(27, fornecedor.length());
			}
			
			log.info("C:" + auxString);
			String[] pree = auxString.split("\""); // name fornecedor
			List<String> pree_list = Arrays.asList(pree);
			
			String fornecedorName = pree_list.get(0);
			Integer preco = Integer.valueOf(pree_list.get(4));
				
			newStructure.put(fornecedorName, preco); 
	        
	        //"idFornecedor":"1","nome":"FNAC","preco":"20"
	        log.info(pree_list.get(0));
	        log.info(pree_list.get(4));
		}
			
		count = 0;
		int precoMaisBarato = 0;
		for (Integer value : newStructure.values()) {
			count++;
			if(count == 1) { // O preço mais barato vai ser sempre o primeiro preço encontrado
				precoMaisBarato = value;
			} else { // Após isso compara sempre os preços entre si
				if(precoMaisBarato < value) {
			    	precoMaisBarato = value;
			    }
			}
		}
		
		delegateTask.setVariable(PRECO_FINAL, precoMaisBarato);
	}
}
