package org.wso2.bpmn.listaperitos.v1;

import org.activiti.engine.delegate.DelegateTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.delegate.Expression;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SelectedInfoListener implements TaskListener {

		private static final long serialVersionUID = 1L;
		
		private static final String PERITOS_SELECIONADOS = "peritosSelecionados";
		private static final String TODOS_OS_PERITOS = "peritosArray";

		private static Log log = LogFactory.getLog(SelectedInfoListener.class);

		public Expression variableNameExpression;

		@SuppressWarnings("unchecked")
		public void notify(DelegateTask delegateTask) {
			log.info("A recolher peritos disponíveis...");
			
			// String separada por vírgulas com os peritos selecionados pelo proponente
			String peritosSelecionadosProponente = delegateTask.getVariable(PERITOS_SELECIONADOS, String.class);
			
			// ArrayList com todos os peritos (utilizadores do sistema)
			List<String> todosPeritos = new ArrayList<>();

			if (delegateTask.hasVariable(TODOS_OS_PERITOS)) {
				todosPeritos = (List<String>) delegateTask.getVariable(TODOS_OS_PERITOS);
			}
			
			// Separar a string e transformar num array list
			String[] arrayPeritos = peritosSelecionadosProponente.split(",");
			List<String> arrayPeritosJaSelecionados = Arrays.asList(arrayPeritos);
						
			todosPeritos.removeAll(arrayPeritosJaSelecionados); // A lista de todos os peritos passa agora a ter apenas os peritos que ainda não foram escolhidos pelo proponente
			
			List <String> peritosFinal = new ArrayList<>(); // Lista que contém os peritos ainda disponíveis para escolha pelo bibliotecário - formato em json para depois ser injetado no simpleSelect
			
			for (String perito : todosPeritos) {
				peritosFinal.add("{\"id\":\"" + perito + "\",\"nome\":\"" + perito + "\"}");
				log.info(perito);
			}
			delegateTask.setVariable("peritosFinal", peritosFinal);		
		}
}
