package org.wso2.bpmn.listaperitos.v1.Configurations;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;  // Import the IOException class to handle errors
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class SaveInfos implements JavaDelegate {

	private static Log log = LogFactory.getLog(SaveInfos.class);

	public Expression variableNameExpression;
	
	private static final String POLOS = "polos";
	private static final String AREAS_CONHECIMENTO = "areasConhecimento";
	
	@Override
	public void execute(DelegateExecution delegateTask) throws Exception {
		// TODO Auto-generated method stub
		log.info("Guarda as infos vindas ndo rest num ficheiro de backup");
		
		Path currentRelativePath = Paths.get("");
		String absolutePath = currentRelativePath.toAbsolutePath().toString();
		
		// Temos polos para guardar num ficheiro 
		// E temos areas de conhecimento para guardar num ficheiro
		
		// Diretorio 
		File directory = new File(absolutePath + "/conf/backup");
        if (!directory.exists()) {
            if (directory.mkdir()) {
                log.info("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        
        // Verifica se já existe uma versão dos ficheiros - se existir apaga
        String filenamePolos = absolutePath + "/conf/backup/list-polos.txt";
        String filenameAreasConhecimento = absolutePath + "/conf/backup/list-areas.txt";
        
		File filePolos = new File(filenamePolos); 
        File fileAreasConhecimento = new File(filenameAreasConhecimento);
        
        if(filePolos.delete()) { 
            log.info("File deleted successfully"); 
        }
        if(fileAreasConhecimento.delete()) { 
            log.info("File deleted successfully"); 
        }
        
		savePolos(filenamePolos, delegateTask);
		saveAreasConhecimento(filenameAreasConhecimento, delegateTask);
		
	}
	
	public void savePolos(String filenamePolos, DelegateExecution delegateTask) {                                               
		// Guardar pólos
		try {		
			
			FileWriter myWriter = new FileWriter(filenamePolos);
	      
			String polos = (String) delegateTask.getVariable(POLOS).toString();
			
			myWriter.write(polos);
			myWriter.close();
			log.info("Successfully wrote to the file.");
		} catch (IOException e) {
			log.error(e);
	    }
    }
	
	public void saveAreasConhecimento(String filenameAreasConhecimento, DelegateExecution delegateTask) {                                               
		// Guardar áreas de conhecimento
		try {		
			
			FileWriter myWriter = new FileWriter(filenameAreasConhecimento);
	      
			String areasConhecimento = (String) delegateTask.getVariable(AREAS_CONHECIMENTO).toString();
			
			myWriter.write(areasConhecimento);
			myWriter.close();
			log.info("Successfully wrote to the file.");
		} catch (IOException e) {
			log.error(e);
	    }
    }
	
}
