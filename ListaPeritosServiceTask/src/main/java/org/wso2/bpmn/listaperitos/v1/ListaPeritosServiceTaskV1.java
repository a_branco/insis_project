package org.wso2.bpmn.listaperitos.v1;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ListaPeritosServiceTaskV1 implements JavaDelegate {

	private static Log log = LogFactory.getLog(ListaPeritosServiceTaskV1.class);
	
	public void execute(DelegateExecution execution) throws Exception {
	
		log.info("looking for registered users...");

		ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
		List<User> users = processEngine.getIdentityService().createUserQuery().orderByUserId().asc().list();
		List<String> registeredUsers = new ArrayList<>();
		List<String> peritosArray = new ArrayList<>();
		
		int count = 0;
		
		for (User user : users) {
			count++;
			log.info("user: " + user.getId());
			registeredUsers.add("{\"id\":\"" + user.getId() + "\",\"nome\":\"" + user.getId() + "\"}");
			peritosArray.add(user.getId());
		}

		execution.setVariable("registeredUsers", registeredUsers); // Todos os peritos (utilizadores do sistema) em formato json
		execution.setVariable("peritosArray", peritosArray); // Array List com os nomes dos peritos
		
		String timer = (String) execution.getVariable("timerProporAquisicao");
		log.info(timer);
	}
}