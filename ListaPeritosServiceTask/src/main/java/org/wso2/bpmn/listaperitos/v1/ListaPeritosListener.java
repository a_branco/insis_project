package org.wso2.bpmn.listaperitos.v1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ListaPeritosListener implements TaskListener {

	private static final long serialVersionUID = 1L;
	
	private static final String PERITOS_SELECIONADOS_BIBLIOTECARIO = "peritosBibliotecario";
	private static final String PERITOS_SELECIONADOS_PROPONENTE = "peritosSelecionados";
	private static final String N_TOTAL_PERITOS = "numberPeritos";
	
	private static Log log = LogFactory.getLog(ListaPeritosListener.class);

	public Expression variableNameExpression;

	@SuppressWarnings("unchecked")
	public void notify(DelegateTask delegateTask) {
		log.info("A criar lista com todos os peritos escolhidos...");
		
		// String separada por vírgulas com os peritos selecionados pelo proponente
		String peritosSelecionadosBibliotecario = delegateTask.getVariable(PERITOS_SELECIONADOS_BIBLIOTECARIO, String.class);
		String peritosSelecionadosProponente = delegateTask.getVariable(PERITOS_SELECIONADOS_PROPONENTE, String.class);
		
		// Separar a string e transformar num array list
		String[] arrayPeritosBibliotecario = peritosSelecionadosBibliotecario.split(",");
		List<String> arrayPeritosJaSelecionadosBibliotecario = Arrays.asList(arrayPeritosBibliotecario);
					
		String[] arrayPeritosProponente = peritosSelecionadosProponente.split(",");
		List<String> arrayPeritosJaSelecionadosProponente = Arrays.asList(arrayPeritosProponente);
		
		List <String> arrayTodosPeritosEscolhidos = new ArrayList<>();
		
		for (String perito : arrayPeritosJaSelecionadosProponente) {
			arrayTodosPeritosEscolhidos.add(perito);
		}
		
		for (String perito : arrayPeritosJaSelecionadosBibliotecario) {
			arrayTodosPeritosEscolhidos.add(perito);
		}
		
		delegateTask.setVariable("todosPeritos", arrayTodosPeritosEscolhidos);
		
		log.info("Todos os peritos: ");
		int number = 0;
		
		for (String perito : arrayTodosPeritosEscolhidos) {
			log.info(perito);
			number++;
		}
		
		delegateTask.setVariable(N_TOTAL_PERITOS, number);
	}

}
